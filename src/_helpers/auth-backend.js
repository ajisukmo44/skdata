import axios from 'axios';
const API_URL = process.env.VUE_APP_API_URL;

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'auth/login', {
        email: user.email,
        password: user.password,
        dashboard: true
      })
      .then(response => {
        console.log('login', response);
        if (response.status == 200) {
          localStorage.setItem('user', JSON.stringify(response.data.data.user));
          localStorage.setItem('token', JSON.stringify(response.data.data.access_token));
        }
        return response.data
        this.$router.go('/');
      });
  }

  logout() {
    localStorage.removeItem('user');
  }
}

export default new AuthService();