<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\TeamSkuad;
use App\Models\Team;
use App\Models\Official;
use Dotenv\Util\Str;
use Illuminate\Http\Request;

class OfficialController extends Controller
{       
    public function index(Request $request){
        $per_page = $request->get('per_page') ?? 10;
        $search = $request->get('search');
    
        $pmn = Official::with(['posisi']);
        if($pmn){
            $pmn->where('nama', 'like', '%' .  $search . '%');
        }
        return ResponseFormatter::success(
            $pmn->paginate($per_page),
            'Data Official Berhasil Diambil'
        );    
    }
    
      public function showBySkuad($id){    
        $Official = Official::with(['posisi'])->where('team_skuad_id', $id);
        return response()->json([
            'success'   => true,
            'message'   => 'Official is Empty',
            'data'      => $Official->get(),
        ], 200);
    }
           
    public function show(Official $pmn, $id){          
        $pmn = Official::with(['skuadteam', 'posisi'])->find($id);
        return ResponseFormatter::success(
            $pmn,
            'Data Official Berhasil Diambil'
        );
    }
    
    public function dropdown(Official $pmn, $id){          
        $pmn = Official::with(['posisi'])->where('team_skuad_id', '!=', $id)->get();
        return ResponseFormatter::success(
            $pmn,
            'Data Official Berhasil Diambil'
        );
    }

    public function store(Request $request) 
    {
        $request->validate([
            'nama'  => 'required',
            'img'   => 'image|mimes:png,jpg,jpeg|max:4048'
        ]);
    
        $acak = rand(100,999);
        $namafoto = $acak ."-". $request->img->getClientOriginalName(); 
        $namafoto1 = '/storage/images/Official/'.$namafoto; 
        
        $Official = Official::create([ 
        'team_skuad_id' => $request->team_skuad_id,
        'img'           => $namafoto1,
        'nama'          => $request->nama,
        'tempat_lahir'  => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'posisi'        => $request->posisi,
        'statistik'     => $request->statistik,
        'keterangan'    => $request->keterangan,
        ]);
    
        if ($request->file('img')) {
            $image      = $request->file('img');
            $fileName   = $namafoto;
            $folder     = Official::UPLOAD_FOLDER;
            $path       = $image->storeAs($folder, $fileName, 'public');
        }
        return ResponseFormatter::success($Official, 'Official Berhasil Ditambahkan');
    }
        
    public function update(Request $request, Official $Official, $id){
        $data   = $request->all();
        $idOfficial  = Official::where('id', $id)->first()->id;
        
        $request->validate([
            'nama'  => 'required',
            'img'   => 'image|mimes:png,jpg,jpeg|max:4048'
        ]);

         
        if ($request->file('img')) {
            $acak = rand(100,999);
            $namafoto = $acak ."-". $request->img->getClientOriginalName(); 
            $namafoto1 = '/storage/images/Official/'.$namafoto; 
    
            $Official->where('id', $idOfficial)->update([
                'team_skuad_id' => $request->team_skuad_id,
                'img'           => $namafoto1,
                'nama'          => $request->nama,
                'no_punggung'   => $request->no_punggung,
                'umur'          => $request->umur,
                'tempat_lahir'  => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'posisi'        => $request->posisi,
                'statistik'     => $request->statistik,
                'keterangan'    => $request->keterangan
             ]);
         
            $image      = $request->file('img');
            $fileName   = $namafoto;
            $folder     = Official::UPLOAD_FOLDER;
            $path       = $image->storeAs($folder, $fileName, 'public');
        } else {
          $Official->where('id', $idOfficial)->update([
            'team_skuad_id' => $request->team_skuad_id,
            'nama'          => $request->nama,
            'no_punggung'   => $request->no_punggung,
            'umur'          => $request->umur,
            'tempat_lahir'  => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'posisi'        => $request->posisi,
            'statistik'     => $request->statistik,
            'keterangan'    => $request->keterangan
         ]);
            
        }

        return ResponseFormatter::success($Official, 'Data Berhasil Diupdate');
    }
    
    public function destroy(Official $Official, $id){
        $pmn  = Official::find($id);
        $imgName = $pmn['images'];

        if($imgName){
            $image_path  = substr($imgName, 1, 100);
            unlink($image_path);
        }
        $Official->where('id', $pmn->id)->delete();
        
        return ResponseFormatter::success($id, 'Data Berhasil Dihapus');
    }        
}
